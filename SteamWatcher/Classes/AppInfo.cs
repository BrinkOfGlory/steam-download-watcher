﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Threading;

namespace SteamDownloadWatcher.Classes {

    public class AppInfo : INotifyPropertyChanged {
        private const int RequestTimeout = 30; // In seconds
        private const string SteamAppdetailsUrl = "http://store.steampowered.com/api/appdetails?appids={0}";

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion Property Changed

        private bool _requestedName = false;

        private uint _steamId;
        public uint SteamId {
            get { return _steamId; }
            set {
                _steamId = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayName));
            }
        }

        private string _name = null;
        public string Name {
            get { return _name; }
            set {
                _name = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(DisplayName));
            }
        }

        public string DisplayName => Name ?? SteamId.ToString();

        public AppInfo(uint steamid) {
            _steamId = steamid;
            if (Properties.Settings.Default.FetchNames) {
                RequestAppName();
            }
        }

        public void RequestAppName() {
            if (!_requestedName) {
                _requestedName = true;

                var worker = new BackgroundWorker();
                worker.DoWork += (se, e) => {
                    var r = WebRequest.Create(String.Format(SteamAppdetailsUrl, _steamId));
                    r.Timeout = RequestTimeout * 1000;
                    try {
                        using (var response = (HttpWebResponse)r.GetResponse()) {
                            using (var reader = new StreamReader(response.GetResponseStream())) {
                                string serverResponse = reader.ReadToEnd();

                                // we cant have numbers as property names. replace it with something we can use
                                // Workaround-oriented programming ಠ_ಠ
                                var regex = new Regex(Regex.Escape(_steamId.ToString()));
                                serverResponse = regex.Replace(serverResponse, "App", 1);

                                dynamic d = Codeplex.Data.DynamicJson.Parse(serverResponse, reader.CurrentEncoding);
                                if (d.App.success) {
                                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => {
                                        Name = d.App.data.name;
                                    }));
                                }
                            }
                        }
                    } catch (Exception ex) {
                        // we dont want this to interrupt the app in any way so just discard it
                        Debug.WriteLine(ex.ToString());
                    }
                };
                worker.RunWorkerAsync();
            }
        }
    }
}