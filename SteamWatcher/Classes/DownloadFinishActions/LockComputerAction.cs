﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class LockComputerAction : IDownloadFinishAction {
        public string Name => "Lock Computer";

        public void Execute(string argument = "") {
            NativeMethods.LockWorkStation();
        }
    }
}