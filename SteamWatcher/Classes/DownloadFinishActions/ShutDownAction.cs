﻿using System.Diagnostics;

namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class ShutDownAction : IDownloadFinishAction {
        public string Name => "Shut Down";

        public void Execute(string argument = "") {
            Process.Start("shutdown", "/s /t 0");
        }
    }
}