﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class HibernateAction : IDownloadFinishAction {
        public string Name => "Hibernate";

        public void Execute(string argument = "") {
            NativeMethods.SetSuspendState(true, true, true);
        }
    }
}