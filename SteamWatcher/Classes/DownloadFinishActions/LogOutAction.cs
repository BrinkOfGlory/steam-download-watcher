﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public class LogOutAction : IDownloadFinishAction {
        public string Name => "Log Out";

        public void Execute(string argument = "") {
            NativeMethods.ExitWindowsEx(0, 0);
        }
    }
}