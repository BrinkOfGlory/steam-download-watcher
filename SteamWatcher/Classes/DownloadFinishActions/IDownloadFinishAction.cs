﻿namespace SteamDownloadWatcher.Classes.DownloadFinishActions {

    public interface IDownloadFinishAction {
        string Name { get; }

        void Execute(string argument = "");
    }
}