﻿using Microsoft.Win32;

namespace SteamDownloadWatcher.Classes {

    public static class SteamInfo {
        public static RegistryKey SteamAppsSubKey { get; private set; }

        static SteamInfo() {
            SteamAppsSubKey = Registry.CurrentUser.OpenSubKey(@"Software\Valve\Steam\Apps");
        }
    }
}